[[_TOC_]]

## Network calls

### (Stop mocking fetch)[https://kentcdodds.com/blog/stop-mocking-fetch]

See [msw](https://github.com/mswjs/msw)

> "_I found MSW and was thrilled that not only could I still see the mocked responses in my DevTools, but that the mocks didn't have to be written in a Service Worker and could instead live alongside the rest of my app. This made it silly easy to adopt. The fact that I can use it for testing as well makes MSW a huge productivity booster._"
>
> – [Kent C. Dodds](https://twitter.com/kentcdodds)

## Test data
See [test-data-bot](https://www.npmjs.com/package/@jackfranklin/test-data-bot)

You may easily build valid objects despite using some that are partially valid just to meet test requirement (which may change!)

May be used together with [faker](https://github.com/faker-js/faker)